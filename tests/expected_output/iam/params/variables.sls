include:
- common-variables
local_tags:
  Automation: 'True'
  COGS: OPEX
  Environment: test-dev
  KubernetesCluster: idem-test
  Owner: org1
