aws_iam_role.cluster:
  aws.iam.role.absent:
  - name: idem-test-temp-xyz-cluster
aws_iam_role_policy_attachment.cluster-AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy
aws_iam_role_policy_attachment.cluster-AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy
aws_security_group.cluster:
  aws.ec2.security_group.absent:
  - name: idem-test-temp-xyz-cluster
aws_security_group.cluster-node:
  aws.ec2.security_group.absent:
  - name: idem-test-temp-xyz-cluster-node
aws_security_group.cluster-node-rule-0:
  aws.ec2.security_group_rule.absent:
  - name: sgr-06fa8980bf9bffea7
aws_security_group.cluster-node-rule-1:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0746837a711b2f632
aws_security_group.cluster-node-rule-2:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0a410f396195a1a29
aws_security_group.cluster-node-rule-3:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0b6d618ed84f17b83
aws_security_group.cluster-node-rule-4:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0d08ef367d62e82f3
aws_security_group.cluster-node-rule-5:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0ffb22f7ca7a0f16c
aws_security_group.cluster-rule-0:
  aws.ec2.security_group_rule.absent:
  - name: sgr-077c2651cc2eb9b1f
aws_security_group.cluster-rule-1:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0a4ef3a78cdce5042
