igw-0eee9bba485b312a8:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: vpc-0738f2a523f4735bd
    name: igw-0eee9bba485b312a8
    resource_id: igw-0eee9bba485b312a8
    tags:
    - Key: Name
      Value: idem-test-temp-xyz
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    vpc_id:
    - vpc-0738f2a523f4735bd


igw-89798ae0:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: vpc-dcae57b5
    name: igw-89798ae0
    resource_id: igw-89798ae0
    vpc_id:
    - vpc-dcae57b5
