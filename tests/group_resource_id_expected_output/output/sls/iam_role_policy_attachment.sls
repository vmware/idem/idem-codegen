AWS-QuickSetup-StackSet-Local-ExecutionRole-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: AWS-QuickSetup-StackSet-Local-ExecutionRole
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


AWSServiceRoleForAWSCloud9-arn:aws:iam::aws:policy/aws-service-role/AWSCloud9ServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAWSCloud9
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSCloud9ServiceRolePolicy


AWSServiceRoleForAmazonBraket-arn:aws:iam::aws:policy/aws-service-role/AmazonBraketServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonBraket
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonBraketServiceRolePolicy


AWSServiceRoleForAmazonElasticFileSystem-arn:aws:iam::aws:policy/aws-service-role/AmazonElasticFileSystemServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonElasticFileSystem
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonElasticFileSystemServiceRolePolicy


AWSServiceRoleForAmazonGuardDuty-arn:aws:iam::aws:policy/aws-service-role/AmazonGuardDutyServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonGuardDuty
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonGuardDutyServiceRolePolicy


AWSServiceRoleForAmazonMQ-arn:aws:iam::aws:policy/aws-service-role/AmazonMQServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonMQ
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonMQServiceRolePolicy


AWSServiceRoleForAmazonOpenSearchService-arn:aws:iam::aws:policy/aws-service-role/AmazonOpenSearchServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonOpenSearchService
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonOpenSearchServiceRolePolicy


AWSServiceRoleForAmazonSSM-arn:aws:iam::aws:policy/aws-service-role/AmazonSSMServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonSSM
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonSSMServiceRolePolicy


AWSServiceRoleForAmazonxyz-arn:aws:iam::aws:policy/aws-service-role/AmazonxyzServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonxyz
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonxyzServiceRolePolicy


AWSServiceRoleForAmazonxyzForFargate-arn:aws:iam::aws:policy/aws-service-role/AmazonxyzForFargateServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonxyzForFargate
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonxyzForFargateServiceRolePolicy


AWSServiceRoleForAmazonxyzNodegroup-arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForAmazonxyzNodegroup:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonxyzNodegroup
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForAmazonxyzNodegroup


? AWSServiceRoleForApplicationAutoScaling_DynamoDBTable-arn:aws:iam::aws:policy/aws-service-role/AWSApplicationAutoscalingDynamoDBTablePolicy


: aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSApplicationAutoscalingDynamoDBTablePolicy


AWSServiceRoleForAutoScaling-arn:aws:iam::aws:policy/aws-service-role/AutoScalingServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAutoScaling
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AutoScalingServiceRolePolicy


AWSServiceRoleForBackup-arn:aws:iam::aws:policy/aws-service-role/AWSBackupServiceLinkedRolePolicyForBackup:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForBackup
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSBackupServiceLinkedRolePolicyForBackup


AWSServiceRoleForCloudWatchEvents-arn:aws:iam::aws:policy/aws-service-role/CloudWatchEventsServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForCloudWatchEvents
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/CloudWatchEventsServiceRolePolicy


AWSServiceRoleForComputeOptimizer-arn:aws:iam::aws:policy/aws-service-role/ComputeOptimizerServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForComputeOptimizer
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/ComputeOptimizerServiceRolePolicy


AWSServiceRoleForEC2Spot-arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForEC2Spot
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotServiceRolePolicy


AWSServiceRoleForEC2SpotFleet-arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotFleetServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForEC2SpotFleet
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotFleetServiceRolePolicy


AWSServiceRoleForECS-arn:aws:iam::aws:policy/aws-service-role/AmazonECSServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForECS
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonECSServiceRolePolicy


AWSServiceRoleForEMRCleanup-arn:aws:iam::aws:policy/aws-service-role/AmazonEMRCleanupPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForEMRCleanup
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonEMRCleanupPolicy


AWSServiceRoleForElastiCache-arn:aws:iam::aws:policy/aws-service-role/ElastiCacheServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForElastiCache
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/ElastiCacheServiceRolePolicy


AWSServiceRoleForElasticLoadBalancing-arn:aws:iam::aws:policy/aws-service-role/AWSElasticLoadBalancingServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForElasticLoadBalancing
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSElasticLoadBalancingServiceRolePolicy


AWSServiceRoleForGlobalAccelerator-arn:aws:iam::aws:policy/aws-service-role/AWSGlobalAcceleratorSLRPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForGlobalAccelerator
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSGlobalAcceleratorSLRPolicy


AWSServiceRoleForImageBuilder-arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForImageBuilder:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForImageBuilder
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForImageBuilder


AWSServiceRoleForKafka-arn:aws:iam::aws:policy/aws-service-role/KafkaServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForKafka
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/KafkaServiceRolePolicy


? AWSServiceRoleForKeyManagementServiceMultiRegionKeys-arn:aws:iam::aws:policy/aws-service-role/AWSKeyManagementServiceMultiRegionKeysServiceRolePolicy


: aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSKeyManagementServiceMultiRegionKeysServiceRolePolicy


AWSServiceRoleForOrganizations-arn:aws:iam::aws:policy/aws-service-role/AWSOrganizationsServiceTrustPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForOrganizations
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSOrganizationsServiceTrustPolicy


AWSServiceRoleForRDS-arn:aws:iam::aws:policy/aws-service-role/AmazonRDSServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForRDS
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonRDSServiceRolePolicy


AWSServiceRoleForRedshift-arn:aws:iam::aws:policy/aws-service-role/AmazonRedshiftServiceLinkedRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForRedshift
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonRedshiftServiceLinkedRolePolicy


AWSServiceRoleForSecurityHub-arn:aws:iam::aws:policy/aws-service-role/AWSSecurityHubServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForSecurityHub
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSSecurityHubServiceRolePolicy


AWSServiceRoleForSupport-arn:aws:iam::aws:policy/aws-service-role/AWSSupportServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForSupport
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSSupportServiceRolePolicy


AWSServiceRoleForTrustedAdvisor-arn:aws:iam::aws:policy/aws-service-role/AWSTrustedAdvisorServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForTrustedAdvisor
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSTrustedAdvisorServiceRolePolicy


AWSServiceRoleForVPCTransitGateway-arn:aws:iam::aws:policy/aws-service-role/AWSVPCTransitGatewayServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForVPCTransitGateway
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSVPCTransitGatewayServiceRolePolicy


AmazonxyzEBSCSIRole-arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy:
  aws.iam.role_policy_attachment.present:
  - role_name: AmazonxyzEBSCSIRole
  - policy_arn: arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy


CloudHealth_Borathon-arn:aws:iam::123456789012:policy/CHTPolicy_Borathon:
  aws.iam.role_policy_attachment.present:
  - role_name: CloudHealth_Borathon
  - policy_arn: arn:aws:iam::123456789012:policy/CHTPolicy_Borathon


CrossAccountSign-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: CrossAccountSign
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


Executelambda-arn:aws:iam::123456789012:policy/EC2ListResources:
  aws.iam.role_policy_attachment.present:
  - role_name: Executelambda
  - policy_arn: arn:aws:iam::123456789012:policy/EC2ListResources


Executelambda-arn:aws:iam::aws:policy/AWSLambdaExecute:
  aws.iam.role_policy_attachment.present:
  - role_name: Executelambda
  - policy_arn: arn:aws:iam::aws:policy/AWSLambdaExecute


Executelambda-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: Executelambda
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


Fori18ntest_+=,.@--arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_:
  aws.iam.role_policy_attachment.present:
  - role_name: Fori18ntest_+=,.@-
  - policy_arn: arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_


Idem_iam_vj_test-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: Idem_iam_vj_test
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


Idem_iam_vj_test-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: Idem_iam_vj_test
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


KK+=HAHA-arn:aws:iam::123456789012:policy/KK=+WW_EES:
  aws.iam.role_policy_attachment.present:
  - role_name: KK+=HAHA
  - policy_arn: arn:aws:iam::123456789012:policy/KK=+WW_EES


MyAppAdmin-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73:
  aws.iam.role_policy_attachment.present:
  - role_name: MyAppAdmin
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73


MyAppAdmin-arn:aws:iam::aws:policy/service-role/AWSConfigRulesExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: MyAppAdmin
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSConfigRulesExecutionRole


ReadOnlyEC2-arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: ReadOnlyEC2
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess


S3BucketAccess-arn:aws:iam::aws:policy/AmazonS3FullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: S3BucketAccess
  - policy_arn: arn:aws:iam::aws:policy/AmazonS3FullAccess


SecureState-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: SecureState
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


SecureStateRole-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: SecureStateRole
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


StatesExecutionRole-arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: StatesExecutionRole
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy


VMWMasterReadOnlyRole-arn:aws:iam::aws:policy/ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: VMWMasterReadOnlyRole
  - policy_arn: arn:aws:iam::aws:policy/ReadOnlyAccess


Wavefront-arn:aws:iam::aws:policy/ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: Wavefront
  - policy_arn: arn:aws:iam::aws:policy/ReadOnlyAccess


? abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


? abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


? abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


? abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


? abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


? abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


afilipov-arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite


afilipov-arn:aws:iam::aws:policy/AWSLambdaFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AWSLambdaFullAccess


afilipov-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


afilipov-arn:aws:iam::aws:policy/AdministratorAccess-Amplify:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess-Amplify


afilipov-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess


afilipov-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


afilipov-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


apigateway-sqs-access-role-arn:aws:iam::123456789012:policy/AzureEventQueuePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: apigateway-sqs-access-role
  - policy_arn: arn:aws:iam::123456789012:policy/AzureEventQueuePolicy


apigateway-sqs-access-role-arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs:
  aws.iam.role_policy_attachment.present:
  - role_name: apigateway-sqs-access-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs


aws-ec2-spot-fleet-autoscale-role-arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetAutoscaleRole:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-ec2-spot-fleet-autoscale-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetAutoscaleRole


aws-ec2-spot-fleet-tagging-role-arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-ec2-spot-fleet-tagging-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole


aws-elasticbeanstalk-ec2-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-ec2-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker


aws-elasticbeanstalk-ec2-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-ec2-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier


aws-elasticbeanstalk-ec2-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-ec2-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier


aws-elasticbeanstalk-service-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkManagedUpdatesCustomerRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-service-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkManagedUpdatesCustomerRolePolicy


aws-elasticbeanstalk-service-role-arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-service-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth


cluster01-temp-xyz-arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a:
  aws.iam.role_policy_attachment.present:
  - role_name: cluster01-temp-xyz
  - policy_arn: arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a


cluster01-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: cluster01-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


clusterlifecycle.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: clusterlifecycle.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com


? config-role-us-east-1-arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1


: aws.iam.role_policy_attachment.present:
  - role_name: config-role-us-east-1
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1


config-role-us-east-1-arn:aws:iam::aws:policy/service-role/AWSConfigRole:
  aws.iam.role_policy_attachment.present:
  - role_name: config-role-us-east-1
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSConfigRole


control-plane.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com


control-plane.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com


control-plane.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com


control-plane.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com


control-plane.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com


controllers.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: controllers.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com


ecsInstanceRole-arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role:
  aws.iam.role_policy_attachment.present:
  - role_name: ecsInstanceRole
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role


ecsTaskExecutionRole-arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: ecsTaskExecutionRole
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy


? go-test-role-4uc6dizk-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081


: aws.iam.role_policy_attachment.present:
  - role_name: go-test-role-4uc6dizk
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081


? go-test-role-9mmf25d1-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d


: aws.iam.role_policy_attachment.present:
  - role_name: go-test-role-9mmf25d1
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d


idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


idem-test-temp-xyz-cluster-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-test-temp-xyz-cluster-arn:aws:iam::aws:policy/AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM


? java-test-role-wex7ne6b-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8


: aws.iam.role_policy_attachment.present:
  - role_name: java-test-role-wex7ne6b
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8


krisi-temp-arn:aws:iam::123456789012:policy/get-encrypted-parameter:
  aws.iam.role_policy_attachment.present:
  - role_name: krisi-temp
  - policy_arn: arn:aws:iam::123456789012:policy/get-encrypted-parameter


lambdaExecutionRole-arn:aws:iam::123456789012:policy/EC2ListResources:
  aws.iam.role_policy_attachment.present:
  - role_name: lambdaExecutionRole
  - policy_arn: arn:aws:iam::123456789012:policy/EC2ListResources


lambdaExecutionRole-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: lambdaExecutionRole
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


migrationhub-discovery-arn:aws:iam::aws:policy/service-role/AWSMigrationHubDiscoveryAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: migrationhub-discovery
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSMigrationHubDiscoveryAccess


? my-s3-function-role-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a


: aws.iam.role_policy_attachment.present:
  - role_name: my-s3-function-role
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a


? my-s3-function-role-arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6


: aws.iam.role_policy_attachment.present:
  - role_name: my-s3-function-role
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6


? myFirstAPI-role-py7jxvn3-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427


: aws.iam.role_policy_attachment.present:
  - role_name: myFirstAPI-role-py7jxvn3
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427


? myTEST-role-ovnuxvqo-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2


: aws.iam.role_policy_attachment.present:
  - role_name: myTEST-role-ovnuxvqo
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2


my_new_role-arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole:
  aws.iam.role_policy_attachment.present:
  - role_name: my_new_role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole


nodes.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: nodes.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com


nodes.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: nodes.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com


org1_cluster01_potato_dev_k8s_admins-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_cluster01_potato_dev_k8s_admins
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins


org1_cluster01_potato_dev_k8s_readonly-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_cluster01_potato_dev_k8s_readonly
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly


org1_idem_test_potato_dev_k8s_admins-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_admins
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins


org1_idem_test_potato_dev_k8s_admins-arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_admins
  - policy_arn: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins


org1_idem_test_potato_dev_k8s_readonly-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_readonly
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly


org1_idem_test_potato_dev_k8s_readonly-arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_readonly
  - policy_arn: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly


? powershell-role-gj7p5czv-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec


: aws.iam.role_policy_attachment.present:
  - role_name: powershell-role-gj7p5czv
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec


rds-monitoring-role-arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole:
  aws.iam.role_policy_attachment.present:
  - role_name: rds-monitoring-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole


securestate-role-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: securestate-role
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


? spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK-arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8


: aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - policy_arn: arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8


spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


? spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG-arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8


: aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - policy_arn: arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8


spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


? t1-role-cn88ujwv-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf


: aws.iam.role_policy_attachment.present:
  - role_name: t1-role-cn88ujwv
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf


test-arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test:
  aws.iam.role_policy_attachment.present:
  - role_name: test
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test


test-xyz_fargate-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: test-xyz_fargate
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy


tgeorgiev-lambda-dynamodb-arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: tgeorgiev-lambda-dynamodb
  - policy_arn: arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess


tgeorgiev-lambda-dynamodb-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: tgeorgiev-lambda-dynamodb
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


vieworgspolicies-arn:aws:iam::aws:policy/AWSOrganizationsReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: vieworgspolicies
  - policy_arn: arn:aws:iam::aws:policy/AWSOrganizationsReadOnlyAccess


vmw-cloudhealth-role-arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation:
  aws.iam.role_policy_attachment.present:
  - role_name: vmw-cloudhealth-role
  - policy_arn: arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation


vmw-cloudhealth-role-arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy:
  aws.iam.role_policy_attachment.present:
  - role_name: vmw-cloudhealth-role
  - policy_arn: arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy


vmws-config-role-arn:aws:iam::aws:policy/service-role/AWSConfigRole:
  aws.iam.role_policy_attachment.present:
  - role_name: vmws-config-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSConfigRole


xyz-cluster01-admin-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-cluster01-admin
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


xyz-cluster02-fargate-pod-execution-role-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-cluster02-fargate-pod-execution-role
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy


xyz-idem-test-admin-arn:aws:iam::123456789012:policy/xyz-idem-test-admin:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-admin
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-admin


xyz-idem-test-fargate-pod-execution-role-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-fargate-pod-execution-role
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy


xyz-idem-test-jenkins-arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-jenkins
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins


xyzClusterRole-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzClusterRole
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


xyzManageRole-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzManageRole
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


xyzManageRole-arn:aws:iam::aws:policy/AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzManageRole
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy


xyzcluster-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzcluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


? xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2-arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - policy_arn: arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy


? xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0-arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - policy_arn: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy


? xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0-arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - policy_arn: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


? xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy


xyzfargateprofile-role-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzfargateprofile-role
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy
