import io
import json
from collections import ChainMap

import pytest
import yaml
from ruamel.yaml import YAML


@pytest.mark.asyncio
def test_add_jinja_resource(hub):
    jinja_dump_data = io.StringIO()
    sls_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/arg_and_parameterized.sls")
    )
    sls_original_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/sls_original_data.sls")
    )

    hub.tf_idem.RUNS["TF_RESOURCE_MAP"] = json.load(
        open(f"{hub.test.idem_codegen.current_path}/files/terraform_resource_map.json")
    )
    jinja_dump_data = hub.idem_codegen.generator.jinja.init.execute(
        "tf_idem", sls_data, sls_original_data
    )
    assert "{% for k in range(3) %}" in jinja_dump_data.getvalue()
    assert '{% if params.get("create_vpc")  %}' in jinja_dump_data.getvalue()
    assert "{% else %}" in jinja_dump_data.getvalue()
    assert "{% endif %}" in jinja_dump_data.getvalue()
    assert (
        'The attribute "kwargs" has terraform replace format'
        in jinja_dump_data.getvalue()
    )
    yaml1 = YAML(typ="jinja2")
    jinja_sls_attributes = yaml1.load(jinja_dump_data.getvalue())
    # Get the resource with if else condition. While loading a jinja template we convert the "{%" to "#%" or
    # "{{m}}" to "<{m}}", this way yaml won't break while loading a jinja template
    resource_attributes = list(
        jinja_sls_attributes["aws_subnet.cluster-<{m}}"].values()
    )[0]
    resource_map = dict(ChainMap(*resource_attributes))
    assert 'if params["create_vpc"]' in resource_map.get("cidr_block")
    assert "% else %" in resource_map.get("vpc_id")
    assert "% endif %" in resource_map.get("vpc_id")
