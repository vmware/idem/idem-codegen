import json


async def test_convert_v3_to_v4(hub):
    tf_state_file_path = f"{hub.test.idem_codegen.current_path}/files/tf_state_v3.json"
    tf_data = hub.idem_codegen.tool.utils.read_json(tf_state_file_path)

    hub.tf_idem.RUNS["TF_STATE_DATA"] = tf_data
    await hub.tf_idem.compiler._loaded.get("0040_convert_v3_to_v4").stage()

    final_data = hub.tf_idem.RUNS["TF_STATE_DATA"]
    final_data = json.loads(json.dumps(final_data))
    assert final_data["version"] == 4
    assert "modules" not in final_data
    assert "resources" in final_data

    tf_state_v4_file_path = f"{hub.test.idem_codegen.current_path}/unit_test_expected_output/tf_state_v4_expected.json"
    expected_output_resources = hub.idem_codegen.tool.utils.read_json(
        tf_state_v4_file_path
    )
    assert final_data["resources"] == expected_output_resources
