aws_cloudwatch_log_group.redlock_flow_log_group:
  aws.cloudwatch.log_group.present:
  - name: xyz-idem-test_redlock_flow_log_group
  - resource_id: xyz-idem-test_redlock_flow_log_group
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - tags:
      Automation: 'true'
      COGS: OPEX
      Environment: test-dev
      KubernetesCluster: idem-test
      Owner: org1


aws_db_subnet_group.db-subnet-group:
  aws.rds.db_subnet_group.present:
  - db_subnet_group_arn: arn:aws:rds:eu-west-3:123456789012:subgrp:db-subnet-group-idem-test
    db_subnet_group_description: For Aurora rds
    name: db-subnet-group-idem-test
    resource_id: db-subnet-group-idem-test
    subnets:
    - ${aws.ec2.subnet:aws_subnet.cluster-2:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-1:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
    tags:
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-db-subnet-group


aws_eip.nat_eip-0:
  aws.ec2.elastic_ip.present:
  - name: 15.236.223.139
  - resource_id: 15.236.223.139
  - allocation_id: eipalloc-0134ceb9112c887fd
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-eip-0
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev


aws_eip.nat_eip-1:
  aws.ec2.elastic_ip.present:
  - name: 13.37.173.220
  - resource_id: 13.37.173.220
  - allocation_id: eipalloc-001d4219447c325ca
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-natgw-eip-1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test


aws_eip.nat_eip-2:
  aws.ec2.elastic_ip.present:
  - name: 13.38.205.2
  - resource_id: 13.38.205.2
  - allocation_id: eipalloc-01319ee06efe14298
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-eip-2
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test


aws_elasticache_subnet_group.default:
  aws.elasticache.cache_subnet_group.present:
  - name: elasticache-subnet-group-idem-test
  - resource_id: elasticache-subnet-group-idem-test
  - cache_subnet_group_description: For elastcache redis cluster
  - arn: arn:aws:elasticache:eu-west-3:123456789012:subnetgroup:elasticache-subnet-group-idem-test
  - subnet_ids:
    - ${aws.ec2.subnet:aws_subnet.cluster-2:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-1:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
  - tags: []


aws_flow_log.redlock_flow_log:
  aws.ec2.flow_log.present:
  - resource_ids:
    - ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
  - resource_type: VPC
  - resource_id: fl-0d8347dbe88a0027a
  - iam_role: ${aws.iam.role:aws_iam_role.redlock_flow_role:arn}
  - log_group_name: ${aws.cloudwatch.log_group:aws_cloudwatch_log_group.redlock_flow_log_group:resource_id}
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


aws_iam_role.redlock_flow_role:
  aws.iam.role.present:
  - resource_id: xyz-idem-test_redlock_flow_role
  - name: xyz-idem-test_redlock_flow_role
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - id: AROAX2FJ77DC7NUDJJ6DH
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "vpc-flow-logs.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


aws_iam_role_policy.redlock_flow_policy:
  aws.iam.role_policy.present:
  - resource_id: xyz-idem-test_redlock_flow_role-xyz-idem-test_redlock_flow_policy
  - role_name: ${aws.iam.role:aws_iam_role.redlock_flow_role:resource_id}
  - name: xyz-idem-test_redlock_flow_policy
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogGroups", "logs:DescribeLogStreams"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


aws_internet_gateway.cluster:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
    name: igw-0eee9bba485b312a8
    resource_id: igw-0eee9bba485b312a8
    tags:
    - Key: Name
      Value: idem-test-temp-xyz
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'


aws_nat_gateway.nat_gateway-0:
  aws.ec2.nat_gateway.present:
  - name: nat-0a49a65a4bb87370a
  - resource_id: nat-0a49a65a4bb87370a
  - subnet_id: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-0:resource_id}
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-0
  - state: available
  - allocation_id: eipalloc-0134ceb9112c887fd


aws_nat_gateway.nat_gateway-1:
  aws.ec2.nat_gateway.present:
  - name: nat-0c02a1f1d590b5534
  - resource_id: nat-0c02a1f1d590b5534
  - subnet_id: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-1:resource_id}
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-1
  - state: available
  - allocation_id: eipalloc-001d4219447c325ca


aws_nat_gateway.nat_gateway-2:
  aws.ec2.nat_gateway.present:
  - name: nat-076cd14a28acd21b4
  - resource_id: nat-076cd14a28acd21b4
  - subnet_id: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-2:resource_id}
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-2
  - state: available
  - allocation_id: eipalloc-01319ee06efe14298


aws_route_table.cluster-0:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-080a96c3caa4d67bc
      RouteTableId: rtb-0516e0e06d933d9f4
      SubnetId: ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
    name: rtb-0516e0e06d933d9f4
    propagating_vgws: []
    resource_id: rtb-0516e0e06d933d9f4
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: ${aws.ec2.nat_gateway:aws_nat_gateway.nat_gateway-0:resource_id}
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-0
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'


aws_route_table.cluster-1:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0832ce3bb146df047
      RouteTableId: rtb-0ee77d1deb1a1d86a
      SubnetId: ${aws.ec2.subnet:aws_subnet.cluster-1:resource_id}
    name: rtb-0ee77d1deb1a1d86a
    propagating_vgws: []
    resource_id: rtb-0ee77d1deb1a1d86a
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: ${aws.ec2.nat_gateway:aws_nat_gateway.nat_gateway-1:resource_id}
      Origin: CreateRoute
      State: active
    tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-1
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'


aws_route_table.cluster-2:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0ef8af8654a7c1fb3
      RouteTableId: rtb-0b0a3c628c59ac049
      SubnetId: ${aws.ec2.subnet:aws_subnet.cluster-2:resource_id}
    name: rtb-0b0a3c628c59ac049
    propagating_vgws: []
    resource_id: rtb-0b0a3c628c59ac049
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: ${aws.ec2.nat_gateway:aws_nat_gateway.nat_gateway-2:resource_id}
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-xyz-private-2
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'


aws_route_table.xyz_public-0:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0f2b91e5f78d7af47
      RouteTableId: rtb-01e542a8c56c9511f
      SubnetId: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-0:resource_id}
    name: rtb-01e542a8c56c9511f
    propagating_vgws: []
    resource_id: rtb-01e542a8c56c9511f
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: ${aws.ec2.internet_gateway:aws_internet_gateway.cluster:resource_id}
      Origin: CreateRoute
      State: active
    tags:
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-xyz-public-0
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'


aws_route_table.xyz_public-1:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-05b12a843fea97f87
      RouteTableId: rtb-0445912793473da66
      SubnetId: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-1:resource_id}
    name: rtb-0445912793473da66
    propagating_vgws: []
    resource_id: rtb-0445912793473da66
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: ${aws.ec2.internet_gateway:aws_internet_gateway.cluster:resource_id}
      Origin: CreateRoute
      State: active
    tags:
    - Key: Name
      Value: idem-test-xyz-public-1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'


aws_route_table.xyz_public-2:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0e7f80eb1863284da
      RouteTableId: rtb-05bd8f7251c25d82c
      SubnetId: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-2:resource_id}
    name: rtb-05bd8f7251c25d82c
    propagating_vgws: []
    resource_id: rtb-05bd8f7251c25d82c
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: ${aws.ec2.internet_gateway:aws_internet_gateway.cluster:resource_id}
      Origin: CreateRoute
      State: active
    tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Name
      Value: idem-test-xyz-public-2
    - Key: Environment
      Value: test-dev
    vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'


aws_subnet.cluster-0:
  aws.ec2.subnet.present:
  - name: subnet-050732fa4616470d9
  - resource_id: subnet-050732fa4616470d9
  - vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'
  - cidr_block: 10.170.0.0/18
  - availability_zone: eu-west-3a
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Owner
      Value: org1


aws_subnet.cluster-1:
  aws.ec2.subnet.present:
  - name: subnet-05dfaa0d01a337199
  - resource_id: subnet-05dfaa0d01a337199
  - vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'
  - cidr_block: 10.170.64.0/18
  - availability_zone: eu-west-3b
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-temp-xyz-node-private


aws_subnet.cluster-2:
  aws.ec2.subnet.present:
  - name: subnet-039e53122e038d38c
  - resource_id: subnet-039e53122e038d38c
  - vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'
  - cidr_block: 10.170.128.0/18
  - availability_zone: eu-west-3c
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: Owner
      Value: org1


aws_subnet.xyz_public_subnet-0:
  aws.ec2.subnet.present:
  - name: subnet-09cecc8c853637d3b
  - resource_id: subnet-09cecc8c853637d3b
  - vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'
  - cidr_block: 10.170.192.0/20
  - availability_zone: eu-west-3a
  - tags:
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1


aws_subnet.xyz_public_subnet-1:
  aws.ec2.subnet.present:
  - name: subnet-0094b72dfb7ce6131
  - resource_id: subnet-0094b72dfb7ce6131
  - vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'
  - cidr_block: 10.170.208.0/20
  - availability_zone: eu-west-3b
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public


aws_subnet.xyz_public_subnet-2:
  aws.ec2.subnet.present:
  - name: subnet-0d68d61b1ab708d42
  - resource_id: subnet-0d68d61b1ab708d42
  - vpc_id: 'var.create_vpc ? ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id} : ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}'
  - cidr_block: 10.170.224.0/20
  - availability_zone: eu-west-3c
  - tags:
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: kubernetes.io/cluster/idem-test
      Value: shared


aws_vpc.cluster:
  aws.ec2.vpc.present:
  - name: vpc-0738f2a523f4735bd
  - resource_id: vpc-0738f2a523f4735bd
  - instance_tenancy: default
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-cluster-node
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-02ab4e7064a606d2b
      CidrBlock: 10.170.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true


aws_vpc_dhcp_options.vpc_options:
  aws.ec2.dhcp_option.present:
  - name: dopt-052512c363a0a800b
  - resource_id: dopt-052512c363a0a800b
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: xyz-idem-test
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


data.aws_ec2_transit_gateway.region-tgw:
  aws.ec2.transit_gateway.search:
  - filters:
    - name: tag:Name
      value:
      - ${var.profile}-tgw-${var.region}


data.aws_subnets.public_subnets:
  exec.run:
  - path: aws.ec2.subnet.list
  - kwargs:
      tags:
      - name: tags:Name
        value: ${var.public_subnet_name}


data.aws_subnets.pvt_subnets:
  exec.run:
  - path: aws.ec2.subnet.list
  - kwargs:
      tags:
      - name: tags:Name
        value: ${var.pvt_subnet_name}


data.aws_vpc.vpc:
  aws.ec2.vpc.search:
  - filters:
    - name: tags:Name
      value: ${var.VpcName}
